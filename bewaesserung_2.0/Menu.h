#include "Arduino.h"
#include <LiquidCrystal_I2C.h>

class Menu{
  public:
    void open();
    boolean isActive();
    void print();
    void next();
    void prev();
    void back();
    void close();
    void setLcd(LiquidCrystal_I2C* lcd);
    void select(Regelkreis* rk);
    void selectOperation();
    int getLevel();
    int getValue();
  private:
    int level;
    LiquidCrystal_I2C* lcd;
    int selectedItemL1;
    int selectedItemL2;
    int value;
    int operation;   
    Regelkreis* rk;
};

String menuItemsL1[] = {"Tomaten", "Kartoffeln", "Wein", "zuruck"};
String menuItemsL2[] = {"untere Schwelle", "obere Schwelle", "zuruck"};

void Menu::setLcd(LiquidCrystal_I2C* lcd){
  this->lcd = lcd;
}

void Menu::open(){
  this->level = 1;
  this->selectedItemL1 = 0;
  this->selectedItemL2 = 0;
  lcd->clear();
}

void Menu::close(){
  this->level = 0;
  lcd->clear();
}

boolean Menu::isActive(){
  return this->level != 0;
}

void Menu::print(){
  if(level == 1){
    for(int i=0; i<4; i++){
      lcd->setCursor(0,i);
      if(this->selectedItemL1 == i){
        lcd->print(">");
      } else{
        lcd->print(" ");
      }
      lcd->print(menuItemsL1[i]);
    }
  }
  if(level == 2){
    lcd->setCursor(0,0);
    lcd->print(menuItemsL1[selectedItemL1]);
    for(int i=0; i<3; i++){
      lcd->setCursor(1,i+1);
      if(this->selectedItemL2 == i){
        lcd->print(">");
      } else{
        lcd->print(" ");
      }
      lcd->print(menuItemsL2[i]);
    }
  }
  if(level == 3){
    lcd->setCursor(0,0);
    lcd->print(menuItemsL1[selectedItemL1]);
    lcd->setCursor(0,1);
    lcd->print(menuItemsL2[selectedItemL2]);
    lcd->setCursor(2,2);
    lcd->print("> ");
    lcd->print(this->value);
  }
}

void Menu::next(){
  lcd->clear();
  if(level == 1){
    this->selectedItemL1 = (selectedItemL1 + 1) % 4;
  }
  if(level == 2){
    this->selectedItemL2 = (selectedItemL2 + 1) % 3;
  }
  if(level == 3){
    this->value++;
  }
  print();
}

void Menu::prev(){
  lcd->clear();
  if(level == 1){
    this->selectedItemL1 = (selectedItemL1 - 1) % 4;
    if(this->selectedItemL1 == -1){
      this->selectedItemL1 = 3;
    }
  }
  if(level == 2){
    this->selectedItemL2 = (selectedItemL2 - 1) % 3;
    if(this->selectedItemL2 == -1){
      this->selectedItemL2 = 2;
    }
  }
  if(level == 3){
    this->value--;
  }
  print();
}

void Menu::select(Regelkreis* rk){
  lcd->clear();
  this->rk = rk;
  this->level++;
}

void Menu::selectOperation(){
  lcd->clear();
  this->operation = this->getValue();
  if(operation == 0){
    this->value = rk->getUntereSchwelle();
  }
  if(operation == 1){
    this->value = rk->getObereSchwelle();
  }
  if(operation == 2){
    this->back();
    return;
  }
  this->level++;
}

void Menu::back(){
  lcd->clear();
  this->level--;
}

int Menu::getLevel(){
  return level;
}


int Menu::getValue(){
  if(level == 1){
    return this->selectedItemL1;
  }
  if(level == 2){
    return this->selectedItemL2;
  }
  if(level == 3){
    return value;
  }
}
