#include <CD74HC4067.h>
#include "Arduino.h"
#include <EEPROM.h>

class Regelkreis{
public:
  Regelkreis(CD74HC4067* mux, int sensor, int ventil);
  void steuer();
  void setUntereSchwelle(int untereSchwelle);
  void setObereSchwelle(int obereSchwelle);
  void setName(String name);
  void init();
  String getName();
  int getUntereSchwelle();
  int getObereSchwelle();
  void print(LiquidCrystal_I2C* lcd, int zeile);
private:
  int _sensor;
  int _ventil;
  int _fehler;
  bool _offen;
  int _untereSchwelle;
  int _obereSchwelle; 
  void ventilZu();
  void ventilAuf();
  String _name;
  int _messwert;
  int adr_memory;
  CD74HC4067* _mux;
};


int eepromReadInt(int adr) {
  byte low, high;
  low=EEPROM.read(adr);
  high=EEPROM.read(adr+1);
  return low + ((high << 8)&0xFF00);
}

void eepromWriteInt(int adr, int wert) {
  byte low, high;
  low=wert&0xFF;
  high=(wert>>8)&0xFF;
  EEPROM.update(adr, low); 
  EEPROM.update(adr+1, high);
}

Regelkreis::Regelkreis(CD74HC4067* mux, int sensor, int ventil){
  _mux = mux;
  _sensor = sensor;
  _ventil = ventil;
  _fehler = 0;
  _offen = false;
  _name = "";
  this->adr_memory = _sensor * 4;
  _untereSchwelle = eepromReadInt(this->adr_memory);
  _obereSchwelle = eepromReadInt(this->adr_memory+2);
  // erster Start -> noch kein Wert im Speicher -> Vorbelegung + Speicherung
  if(_untereSchwelle <= 0){
    _untereSchwelle = 80;
    eepromWriteInt(this->adr_memory, _untereSchwelle);
  }
  if(_obereSchwelle <= 0){
    _obereSchwelle = 120;
    eepromWriteInt(this->adr_memory+2, _obereSchwelle);
  }
};

void Regelkreis::setUntereSchwelle(int untereSchwelle){
  _untereSchwelle = untereSchwelle;
  eepromWriteInt(this->adr_memory, _untereSchwelle);
}

void Regelkreis::setObereSchwelle(int obererSchwelle){
  _obereSchwelle = obererSchwelle;
  eepromWriteInt(this->adr_memory+2, _obereSchwelle);
}

void Regelkreis::steuer(){
  _mux->channel(_sensor);
  _messwert = analogRead(3);
  if(_messwert < _untereSchwelle && _offen){
    ventilZu();
  }
  if(_messwert > _obereSchwelle && !_offen){
    _fehler++;
  } else {
    _fehler = 0;
  }
  if(_fehler > 3){
    ventilAuf();
  }
}

void Regelkreis::ventilAuf(){
  digitalWrite(_ventil, LOW);
  _offen = true;
}

void Regelkreis::ventilZu(){
  digitalWrite(_ventil, HIGH);
  _offen = false;
}

void Regelkreis::setName(String name){
  _name = name;
}

String Regelkreis::getName(){
  return _name;
}

void Regelkreis::init(){
  pinMode(_ventil, OUTPUT);
  ventilZu();
}


int Regelkreis::getUntereSchwelle(){
  return _untereSchwelle;
}

int Regelkreis::getObereSchwelle(){
  return _obereSchwelle;
}

void Regelkreis::print(LiquidCrystal_I2C* lcd, int zeile){
  lcd->setCursor(0, zeile);
  lcd->print(_name);
  lcd->print(": ");
  lcd->print(map(_messwert,0,1023,100,0));
  lcd->print("%");
  if(_offen){
    lcd->setCursor(15, zeile);
    lcd->print("[AUF]");
  } else {
    lcd->setCursor(16, zeile);
    lcd->print("[ZU]");
  }
  Serial.print(_name);
  Serial.print(":");
  Serial.print(_messwert);
  Serial.print(";OS:");
  Serial.print(_obereSchwelle);
  Serial.print(";US:");
  Serial.print(_untereSchwelle);
  Serial.print(";Fehler:");
  Serial.println(_fehler);
  
  
}
