#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <CD74HC4067.h>
#include <TimerOne.h>
#include <ClickEncoder.h>
#include "Regelkreis.h"
#include "Menu.h"


CD74HC4067 mux(2,3,4,5);
Regelkreis tomaten(&mux, 0, 6); //Rot
Regelkreis kartoffeln(&mux, 1, 7); //Gelb
Regelkreis wein(&mux, 2, 8); //Grün
Regelkreis *kreise[] = {&tomaten, &kartoffeln, &wein};


int menuSelection[] = {0,0,0};

ClickEncoder *encoder;
void timerIsr() {
  encoder->service();
}

LiquidCrystal_I2C lcd(0x27, 20, 4);
Menu menu;
int16_t last, value;

void setup() {
  Serial.begin(9600);
  pinMode(A3, INPUT);
  lcd.init();
  lcd.backlight();
  
  tomaten.init();
  kartoffeln.init();
  wein.init();

  tomaten.setName("Tomaten");
  kartoffeln.setName("Kartoffeln");
  wein.setName("Wein");

  menu.setLcd(&lcd);

  encoder = new ClickEncoder(A1, A0, A2);
  encoder->setAccelerationEnabled(false);
  last = encoder->getValue();
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);
  
}

void loop() {
  if(menu.isActive()){
    loopMenu();
  } else {
    loopDefault();
  }
}

void loopMenu(){
  menu.print();
  int level = menu.getLevel();
  int dir = readRotaryEncoder();
  if(dir == 1){//runter
    menu.next();
  }
  if(dir == -1){//hoch
    menu.prev();
  }
  
  boolean clicked = false;
  ClickEncoder::Button b = encoder->getButton();
   if (b != ClickEncoder::Open) {
   switch (b) {
      case ClickEncoder::Clicked:
        clicked=true;
        break;
    }
  }
  
  if(level == 1 && clicked){
    int val = menu.getValue();
    if(val == 3){
      menu.close();
    } else {
      menu.select(kreise[val]);
      menuSelection[0] = val;
    }
  }

  if(level == 2 && clicked){
    int val = menu.getValue();
    if(val == 2){
      menu.back();
    } else {
      menuSelection[1] = val;
      menu.selectOperation();
    }
  }

  if(level == 3){
    if(!clicked){
      int val = menu.getValue();
      menuSelection[2] = val;
    } else {
      Regelkreis *kreis = kreise[menuSelection[0]];
      if(menuSelection[1] == 0){
        kreis->setUntereSchwelle(menuSelection[2]);
      }
      if(menuSelection[1] == 1){
        kreis->setObereSchwelle(menuSelection[2]);
      }
      menu.back();
    }
  }

  if(clicked){
    delay(150);
  }
}

int readRotaryEncoder()
{
  value += encoder->getValue();
  
  if (value/2 > last) {
    last = value/2;
    return 1;
  }else if (value/2 < last) {
    last = value/2;
    return -1;
  } else {
    return 0;
  }
  
}

void loopDefault(){
    tomaten.steuer();
    kartoffeln.steuer();
    wein.steuer();
    lcd.clear();
    tomaten.print(&lcd,0);
    kartoffeln.print(&lcd,1);
    wein.print(&lcd,2);
  
    int delayVal = 2000;
    while(delayVal > 0 && !menu.isActive()){
      delay(10);
      delayVal--;
      if(encoder->getButton() == ClickEncoder::Clicked){
        menu.open();
        break;
      }
    }
}
